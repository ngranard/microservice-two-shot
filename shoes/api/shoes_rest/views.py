from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "id", "picture_url"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if (bin_vo_id is not None):
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = f"/api/bins/{bin_vo_id}/"
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"error": "Bin does not exist"},
                status = 400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"},status = 400)
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"message": count > 0})
    else:
        content = json.loads(request.body)
        try:
            Shoe.objects.filter(id=pk).update(**content)
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"},
            status = 400)

























    """
    Collection RESTful API handler for Shoe objects in
    the wardrobe.

    GET:
    Returns a dictionary with a single key "shoes" which
    is a list of the manufacturer, model name, color, and
    picture url for the shoe, along with its href and id.

    {
        "shoes": [
            {
                "id": database id for the shoe,
                "manufacturer": shoe's manufacturer,
                "model_name": the name of the shoe model,
                "color": the color of the shoe,
                "picture_url": the URL of the shoe's picture,
                "href": URL to the shoe,
            },
            ...
        ]
    }

    POST:
    Creates a shoe resource and returns its details.
    {
        "manufacturer": shoe's manufacturer,
        "model_name": the name of the shoe model,
        "color": the color of the shoe,
        "picture_url": the URL of the shoe's picture,
    }
    # """
