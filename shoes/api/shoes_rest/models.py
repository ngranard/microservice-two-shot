from django.db import models
from django.urls import reverse


# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=50)
    bin_number = models.CharField(max_length=50)
    bin_size = models.CharField(max_length=50)
    import_href = models.CharField(max_length=50, unique=True)

def __str__(self):
    return f"{self.closet_name} {self.bin_number}"

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
        )


    def get_api_url(self):
        return reverse("api_shoe", kwargs={"pk": self.pk})
