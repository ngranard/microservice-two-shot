import { useEffect, useState } from "react";
import { Link } from "react-router-dom";


function ShoeList() {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (id) => {
        const url = `http://localhost:8080/api/shoes/${id}/`
        const response = await fetch(url, { method: "DELETE" });
        if (response.ok) {
            fetchData();
        } else {
            console.log(`Failed to delete shoe with ID ${id}`);
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-sm">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <div className="card shadow">
                                <div className="card-body">
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Manufacturer</th>
                                                <th>Model Name</th>
                                                <th>Color</th>
                                                <th>Image</th>
                                                <th>Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {shoes.map(shoe => {
                                                return (
                                                    <tr key={shoe['id']}>
                                                        <td>{shoe.manufacturer}</td>
                                                        <td>{shoe.model_name}</td>
                                                        <td>{shoe.color}</td>
                                                        <td>
                                                            <img
                                                                src={shoe.picture_url}
                                                                className="img-thumbnail"
                                                                alt="new"
                                                            />
                                                        </td>
                                                        <td>
                                                            <button
                                                                className="btn btn-danger"
                                                                onClick={() => handleDelete(shoe.id)}
                                                            >
                                                                Delete Shoe
                                                            </button>
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                                        <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a New Shoe</Link>
                                    </div>
                                </div>
                                <div className="col-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default ShoeList;
