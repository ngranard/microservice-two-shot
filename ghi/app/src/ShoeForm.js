import React, { useEffect, useState } from 'react';

function ShoeForm(props) {

    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState('');

    const fetchData = async () => {

        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }



    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.model_name = model_name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;
        console.log(data)

        const binId = document.getElementById('bin');
        const value = binId.value;
        const url = `http://localhost:8080/api/bins/${value}/shoes/`;
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const shoeResponse = await fetch(url, fetchOptions);
        if (shoeResponse.ok) {
            setModelName('');
            setManufacturer('');
            setColor('');
            setBin([]);
            setPictureUrl('');

        }
    }
    const handleChangeModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleChangeBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }




    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <div className="card shadow">
                        <div className="card-body">
                                <h1 className="card-title">Enter a New Shoe</h1>
                            <form onSubmit={handleSubmit} id="create-shoe-form">
                                <p className="mb-3">
                                    Please enter the details of the shoe you would like to add to the wardrobe.
                                </p>
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeManufacturer} required placeholder="Manufacturer" type="text" className="form-control" id="manufacturer" name="manufacturer" value={manufacturer} />
                                    <label htmlFor="manufacturer">Manufacturer</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeModelName} required placeholder="Model Name" type="text" className="form-control" id="model_name" value={model_name} />
                                    <label htmlFor="model_name">Model Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeColor} required placeholder="Color" type="text" className="form-control" id="color" name="color" value={color} />
                                    <label htmlFor="color">Color</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangePictureUrl} required placeholder="PictureUrl" type="text" className="form-control" id="picture_url" name="picture_url" value={picture_url} />
                                    <label htmlFor="picture_url">Picture URL</label>
                                </div>
                                <div className="mb-3">
                                    <select onChange={handleChangeBin} required type="text" className="form-select" id="bin" name="bin" value={bin} >
                                        <option value="">Bin</option>
                                        {bins.map(bin => {
                                            return (
                                                <option key={bin.id} value={bin.id}>
                                                    {bin.closet_name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-lg btn-primary">Submit</button>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default ShoeForm;
