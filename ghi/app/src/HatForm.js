import { useState, useEffect } from 'react'



function HatsForm() {
    const[style, setStyle] = useState('');
    const[fabric, setFabric] = useState('');
    const[color, setColor] = useState('');
    const[picture, setPicture] = useState('');
    const[location, setLocation] = useState('');
    const[locations, setLocations] = useState([]);

    // const handleStyleChange = (e) => {
    //     const value = e.target.value;
    //     setStyle(value)
    // }
    // const handleFabricChange = (e) => {
    //     const value = e.target.value;
    //     setFabric(value)
    // }
    // const handleColorChange = (e) => {
    //     const value = e.target.value;
    //     setColor(value)
    // }
    // const handleLocationChange = (e) => {
    //     const value = e.target.value;
    //     setLocation(value)
    // }
    // const handlePictureChange = (e) => {
    //     const value = e.target.value;
    //     setPicture(value)
    // }

    useEffect(() => {
        async function getLocations() {
            const url = 'http://localhost:8100/api/locations/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log(data)
                setLocations(data.locations);
            }
        }
        getLocations();
    }, []);

    function changeHandler(setter) {
        return function (e) {
            setter(e.target.value);
        }
    }
    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.style = style;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = picture;
        data.location = location;
        console.log(data);

        const hatUrl = `http://localhost:8090/api/hats/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            }
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)
            setStyle('')
            setFabric('')
            setColor('')
            setLocation([])
            setPicture('')
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={changeHandler(setStyle)} value={style} placeholder="Style" required type="text" name="style" id="style" className="form-control"/>
                    <label htmlFor="Style">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={changeHandler(setFabric)} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={changeHandler(setColor)} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={changeHandler(setPicture)} value={picture} placeholder="Picture" required type="text" name="Picture" id="Picture" className="form-control"/>
                    <label htmlFor="Picture">Picture</label>
                </div>
                <div className="mb-3">
                    <select required onChange={changeHandler(setLocation)} name="location" id="location" className="form-select" value={location} >
                        <option value="">Choose a Location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.id} value={location.href}>
                                    {location.closet_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>

            </div>
        </div>
    </div>

    )
}

export default HatsForm
