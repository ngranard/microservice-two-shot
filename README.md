# Wardrobify

Team:
* Brandon Moore - Hats
* Noah Granard - Shoes


## Design


## Shoes microservice
Using React as front-end and Django and the back-end, the application will allow users to create a wardrobe of shoes. The shoes microservice will allow users to create shoes and add them to their wardrobe. The shoes microservice will also allow users to view their shoes and delete shoes from their wardrobe.

- Created a Shoe Model with the following fields: manufacturer, model name, color, a picture URL, and a bin foreignkey.
- Created views for the Shoe Model which in tandem with React and the React Router, enables the user to: see a list of their shoes, create a new shoe, or delete a shoe.
- The shoes microservice polls the wardrobe microservice to find the bin information for each shoe.

## Hats microservice

Hats service is built with LocationVO to track the specifications of style, fabric, color, picture_url and location. Afterwards, React was implemented to display that data, as well as be manipulated by a user through creating or deleting their own hat.
